#Furniture market

Furniture market este un proiect gandit sa dezvolte un magazin online pentru o firma de mobilier la comanda. Problema principala pe care proiectul vrea sa o rezolve este nevoie clientului de a afla un pret provizoriu pentru mobilierul dorit.
Acest proiect urmareste anumite aspecte clare pentru dezvoltarea aplicatiei, cum ar fi:
  - Dezvoltare in Java Spring
  - Utilizare Design Pattern-uri: Facade, Observer, Factory, DAO, Singleton
  - Datele sunt stocate intr-o baza de date, utilizandu-se tehnici de reflectie pentru Object Relational Mapping.

#Cerinte
Magazinul propus prezinta tipurile de lemn pe care clientul le poate alege, tipul de cant, si dimensiunea corupurilor pe care le doreste.Acesta primind instant un pret pentru obiectele cerute fara a fi nevoie deplasarea unei persoane din cadrul firmei la domiciliu.
Sistemul ofera 2 tipuri de utilizator:
1) Administrator
-poate sa insereze, modifice sau streaga culori disponibile;
-poate sa insereze, modifice sau streaga tipuri de cant disponibile;
-poate sa insereze, modifice sau streaga comenzi disponibile;
-administratorul este anuntat cand o comanda noua vine din partea clientului;
2)Clientul
-poate sa vizualizeze toate tipurile de lemn si cant disponibile;
-poate sa realizeze o comanda introducand informatiile despre culoare mobilierului si dimensiunile dorite;

#Specificatii tema
Pentru dezvoltarea acestei teme, s-a urmarit implementarea principiilor programarii orientate pe obiect si tehnicilor fundamentale de programare si implementare software.
Principalele caracteristici pentru dezvoltarea acestui proiect sunt:
 -  Limbaj de programare utilizat: Java, alaturi de framework-ul Spring
 -  Mediu de dezvoltare: IntelliJ
 -  Baza de date a fost implementata cu ajutorul aplicatiei MySQL Workbench, ruland pe MySQL Server
 -  In tema, pentru dezvoltare si integrare, vor fi utilzate Design Pattern-uri in diverse scopuri, cele mai importante fiind: Observer, Facade, Factory, Data Acces Object, Singleton
 -  Aplicatia este multi-level, adica nivelele au fost separate pentru o dezvoltare iterativa si concreta.
 -  Design Pattern-ul Data Acces Object este utilizat pentru separarea nivelurilor de jos pentru accesul la baza de date si clasele de business pentru efectuarea serviciilor.
 -  Design Pattern-ul Facade este utilizat pentru ascunderea complexitatii operatiilor si logicii si pentru usurarea utilizarii sistemului si a mediului integrat.
 -  Se utilizeaza Design Pattern-ul Observer pentru notificarea clientilor in legatura cu anumite actiuni.
 -  Se utilizeaza Design Pattern-ul Factory pentru generearea de raporturi, in functie de alegera administratorului.
 -  Se utilizeaza Mockito pentru testarea si validarea sistemului (JUnit Tests)

#Implementare

Baza de date este compusa din 9 tabele, cele mai importante fiind client, administrator, comanda, corp, culori lemn, tipuri cant etc.

Aplicatiei i-am oferit o structurare bazata pe pachete.
Pachete aplicatiei sunt:
-DAO, pentru a facilita accesul la BD si executarea operatiilor aferente acesteia
-Conncetion, ne ofera conexiunea propriu zisa cu BD.
-BLL, este un business layer unde se realizeaza operatiile low-level.
-Model, unde sunt implementate toate clasele aferente tabelelor din BD.
-Facade, ne ajute in implementarea design pattern-ului facade.



License
----
MIT