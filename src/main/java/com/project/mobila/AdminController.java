package com.project.mobila;

import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class AdminController extends BaseController {


    @PostMapping("/insertCorp")
    public void insertCorp(@RequestBody Corp corp) {
        info.insertCorp(corp);
    }

    @PostMapping("/updateCorp/{id}")
    public void updateCorp(@PathVariable int id,@RequestBody Corp update) {
        info.updateCorp(id,update);
    }

    @PostMapping("/deleteCorp")
    public void deteleCorp(@RequestBody int id) {
        info.deleteCorp(id);
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/ComandaById/{id}")
    public Comanda findComandaByID(@PathVariable int id) {
        return info.findComandaByID(id);
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/allComanda")
    public List<Comanda> findAllComanda() {
        return info.findAllComanda();
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/allClient")
    public List<Client> findAllClient() {
        return info.findAllClient();
    }

}
