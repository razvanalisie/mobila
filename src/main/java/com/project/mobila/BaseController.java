package com.project.mobila;

import com.project.mobila.bll.AdminBLL;
import com.project.mobila.bll.ClientBLL;
import com.project.mobila.facade.FacadeClass;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController {
    AdminBLL adminBLL;
    ClientBLL clientBLL;
    FacadeClass info;

    public BaseController() {
        this.adminBLL = new AdminBLL();
        this.clientBLL = new ClientBLL();
        this.info = new FacadeClass(adminBLL, clientBLL);
    }
}
