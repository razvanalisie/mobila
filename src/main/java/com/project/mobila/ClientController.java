package com.project.mobila;

import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class ClientController extends BaseController {

    @CrossOrigin(origins = "*")
    @GetMapping("/allCorp")
    public List<Corp> findAllCorp() {
        return info.findAllCorp();
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/corpById/{id}")
    public Corp findCorpByID(@PathVariable String id) {
        //System.out.println(id);
        return info.findCorpByID(Integer.parseInt(id));
    }

    @PostMapping("/newComanda")
    public void newComanda(@RequestBody Comanda comanda, Client client) {


        info.newComanda(comanda,client);
    }

    @PostMapping("/updateComanda/{id}")
    public void updateComanda(@PathVariable int id,@RequestBody Comanda update) {
        info.updateComanda(id,update);
    }

    @PostMapping("/deleteComanda")
    public void deleteComanda(@RequestBody int id) {
        info.deleteComanda(id);
    }
}
