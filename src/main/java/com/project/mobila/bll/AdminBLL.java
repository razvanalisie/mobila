package com.project.mobila.bll;

import com.project.mobila.dao.ClientDAO;
import com.project.mobila.dao.ComandaDAO;
import com.project.mobila.dao.CorpDAO;
import com.project.mobila.facade.AdminOperations;
import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;

import java.sql.SQLException;
import java.util.List;

public class AdminBLL implements AdminOperations {
    private CorpDAO cantDAO;
    private ComandaDAO comandaDAO;
    private ClientDAO clientDAO;
    public AdminBLL() {

        this.cantDAO = new CorpDAO();
        this.clientDAO = new ClientDAO();
        this.comandaDAO = new ComandaDAO();
    }





    public Corp findCorpByID(int id) {
        return cantDAO.findById(id);
    }

    public List<Corp> findAllCorp() {
        return cantDAO.findAll();
    }


    public void insertCorp(Corp cant) {
        cantDAO.insert(cant);
    }


    public void updateCorp(int id, Corp update) {

        try {
            cantDAO.update(update);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void deleteCorp(int id) {
        cantDAO.deleteById(id);
    }


    public Comanda findComandaByID(int id) {
        return comandaDAO.findById(id);
    }
    public List<Client> findAllClient() {
        return clientDAO.findAll();
    }

    public List<Comanda> findAllComanda() {
        return comandaDAO.findAll();
    }

}