package com.project.mobila.bll;

import com.project.mobila.dao.CorpDAO;
import com.project.mobila.dao.ComandaDAO;
import com.project.mobila.facade.ClientOperations;
import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;

import java.sql.SQLException;
import java.util.List;

public class ClientBLL implements ClientOperations {
    private CorpDAO cantDAO;
    private ComandaDAO comandaDAO;

    public ClientBLL() {
        this.cantDAO = new CorpDAO();
        this.comandaDAO = new ComandaDAO();

    }


    public List<Corp> findAllCorp() {
        return cantDAO.findAll();
    }


    public Corp findCorpByID(int id) {
        return cantDAO.findById(id);
    }


    public void newComanda(Comanda comanda,Client client) {
        comandaDAO.insert(comanda);

    }


    public void updateComanda(int id, Comanda update) {
        try {
            comandaDAO.update(update);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void deleteComanda(int id) {
        comandaDAO.deleteById(id);
    }
}
