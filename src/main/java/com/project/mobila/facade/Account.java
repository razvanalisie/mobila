package com.project.mobila.facade;


import com.project.mobila.model.*;
/**
*Prin aceasta clasa se implementeaza design pattern-ul Factory.
**/
public class Account {


    public Administrator adaugareContAdmin(String nume,String parola,int id)
    {   Administrator account =  new Administrator(id,nume, parola);

        return account;
    }

    public Client adaugareContClient(String nume,String parola,int id)
    {   Client account =  new Client(id,nume, parola);

        return account;
    }
}
