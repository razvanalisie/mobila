package com.project.mobila.facade;

import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;

import java.util.List;

public interface AdminOperations {

    public void insertCorp(Corp cant);
    public void updateCorp(int id, Corp update);
    public void deleteCorp(int id);

    public Comanda findComandaByID(int id);
    public List<Comanda> findAllComanda();

    public List<Client> findAllClient();
}
