package com.project.mobila.facade;

public interface ChannelObserver {
    public void update(String mesage);
}
