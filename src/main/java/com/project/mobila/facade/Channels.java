package com.project.mobila.facade;

import java.util.ArrayList;

public class Channels {
    private String message;
    private ArrayList<ChannelObserver> channels = new ArrayList<ChannelObserver>();

    public void addObserver(ChannelObserver c){
        this.channels.add(c);
    }

    public void removeObserver(ChannelObserver c){
        this.channels.remove(c);
    }

    public void setMsg(String msg){
        this.message = msg;
        for(ChannelObserver channel : channels){
            channel.update(msg);
        }
    }
}
