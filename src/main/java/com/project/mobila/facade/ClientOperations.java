package com.project.mobila.facade;

import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;

import java.util.List;

public interface ClientOperations {

    public List<Corp> findAllCorp();
    public Corp findCorpByID(int id);
    public void newComanda(Comanda comanda,Client client);
    public void updateComanda(int id,Comanda update);
    public void deleteComanda(int id);
}
