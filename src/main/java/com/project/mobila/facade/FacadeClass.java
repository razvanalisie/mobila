package com.project.mobila.facade;

import com.project.mobila.model.Client;
import com.project.mobila.model.Comanda;
import com.project.mobila.model.Corp;

import java.util.List;

public class FacadeClass  {

    private AdminOperations adminOperations;
    private ClientOperations clientOperations;

    public FacadeClass(AdminOperations adminOperations, ClientOperations clientOperations) {
        this.adminOperations = adminOperations;
        this.clientOperations = clientOperations;
    }




    public void insertCorp(Corp cant) {
        adminOperations.insertCorp(cant);
    }


    public void updateCorp(int id, Corp update) {
        adminOperations.updateCorp(id,update);
    }


    public void deleteCorp(int id) {
        adminOperations.deleteCorp(id);
    }


    public Comanda findComandaByID(int id) {
        return adminOperations.findComandaByID(id);
    }


    public List<Comanda> findAllComanda() {
        return adminOperations.findAllComanda();
    }

    public List<Corp> findAllCorp() {
        return clientOperations.findAllCorp();
    }


    public Corp findCorpByID(int id) {
        return clientOperations.findCorpByID(id);
    }


    public void newComanda(Comanda comanda, Client client) {
        clientOperations.newComanda(comanda,client);
    }


    public void updateComanda(int id, Comanda update) {
        clientOperations.updateComanda(id,update);
    }


    public void deleteComanda(int id) {
        clientOperations.deleteComanda(id);
    }

    public List<Client> findAllClient() {return adminOperations.findAllClient();}
}

