package com.project.mobila.model;

import com.project.mobila.facade.ChannelObserver;

import java.util.*;

public class Administrator implements ChannelObserver  {

    private int idAdministrator;
    private String message;
    private String username;
    private String password;

    public Administrator(int idAdministrator, String username, String password) {
        this.idAdministrator = idAdministrator;
        this.username = username;
        this.password = password;
    }

    public int getIdAdministrator() {
        return idAdministrator;
    }

    public void setIdAdministrator(int idAdministrator) {
        this.idAdministrator = idAdministrator;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "idAdministrator=" + idAdministrator +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public void update(String mesage) {
        this.setMessage(mesage);
        System.out.println(this.getMessage());
    }
}
