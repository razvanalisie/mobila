package com.project.mobila.model;

import java.util.Set;

public class Client{
    private int idClient;
    private String username;
    private String password;

    public Client(int idClient, String username, String password) {
        this.idClient = idClient;
        this.username = username;
        this.password = password;
    }
    public Client()
    {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }


}
