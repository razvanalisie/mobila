package com.project.mobila.model;

import java.util.Date;
import java.util.List;

public class Comanda {
    private int id;
    private int idCorp;
    private float pretfinal;
    private int idClient;

    public Comanda( int idCorp, int idComanda, float pret) {
        this.id = idComanda;
        this.idCorp = idCorp;
        this.pretfinal = pret;

    }

    public Comanda() {

    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdCorp() {
        return idCorp;
    }

    public void setIdCorp(int idCorp) {
        this.idCorp = idCorp;
    }

    public int getId() {
        return id;
    }

    public void setId(int idComanda) {
        this.id = idComanda;
    }

    public float getPretfinal() {
        return pretfinal;
    }

    public void setPretfinal(float pret) {
        this.pretfinal= pret;
    }

}
