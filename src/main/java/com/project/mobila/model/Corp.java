package com.project.mobila.model;

public class Corp {
    private int id;
    private String nume;
    private float pret;

    public Corp(int idCorp, String nume, float pret) {
        this.id = idCorp;
        this.nume = nume;
        this.pret = pret;
    }

    public Corp()
    {

    }

    public int getId() {
        return id;
    }

    public void setId(int idCorp) {
        this.id= idCorp;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }
}
