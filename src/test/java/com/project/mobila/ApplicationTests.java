package com.project.mobila;

import com.project.mobila.facade.Account;
import com.project.mobila.facade.AdminOperations;
import com.project.mobila.facade.ClientOperations;
import com.project.mobila.facade.FacadeClass;
import com.project.mobila.model.Administrator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class ApplicationTests {

    @Mock
    AdminOperations adminOperations;

    @Mock
    ClientOperations clientOperations;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    private FacadeClass facadeClass;

    @Before
    public void init(){
        facadeClass = new FacadeClass(adminOperations, clientOperations);
    }

    @Test
    public void testAllCorp(){
        facadeClass.findAllCorp();
        verify(clientOperations).findAllCorp();
    }
    /*
    @Test
    public void TestObserver(){
        Library library = new Library();
        ClientWrapper client = new ClientWrapper();
        library.addObserver(client);
        library.setMsg("Test Observer");

        assertEquals(client.getNews(),"Test Observer");
    }
*/
    @Test
    public void TestFactoryAdmin(){
        Account account= new Account();
        Administrator a = account.adaugareContAdmin("nume","parola",1);
        assertEquals(a.toString(), "Administrator{idAdministrator=1, username='nume', password='parola'}");
    }

}
